tap "atlassian/tap"
tap "ethereum/ethereum"
tap "homebrew/bundle"
tap "homebrew/cask"
tap "homebrew/cask-fonts"
tap "homebrew/core"
tap "homebrew/services"
tap "osrf/simulation"
tap "pivotal/tap"
# Search tool like grep, but optimized for programmers
brew "ack"
# Low-level access to audio, keyboard, mouse, joystick, and graphics
brew "sdl2"
# Library for command-line editing
brew "readline"
# Command-line interface for SQLite
brew "sqlite"
# Java build tool
brew "ant"
# Image format providing lossless and lossy compression for web images
brew "webp"
# Codec library for encoding and decoding AV1 video streams
brew "aom"
# Highly capable, feature-rich programming language
brew "perl"
# Spell checker with better logic than ispell
brew "aspell"
# Tool for generating GNU Standards-compliant Makefiles
brew "automake"
# Programmable completion for Bash 3.2
brew "bash-completion"
# Glorious Glasgow Haskell Compilation System
brew "ghc"
# Command-line interface for Cabal and Hackage
brew "cabal-install"
# GNU compiler collection
brew "gcc"
# AV1 decoder targeted to be small and fast
brew "dav1d"
# GNU Transport Layer Security (TLS) Library
brew "gnutls"
# High quality MPEG Audio Layer III (MP3) encoder
brew "lame"
# Package for scientific computing with Python
brew "numpy"
# Open source computer vision library
brew "opencv"
# Fast open framework for deep learning
brew "caffe"
# GNU File, Shell, and Text utilities
brew "coreutils"
# Readline wrapper: adds readline support to tools that lack it
brew "rlwrap"
# Dynamic, general-purpose programming language
brew "clojure"
# Cross-platform make
brew "cmake"
# Get a file from an HTTP, HTTPS or FTP server
brew "curl"
# CLI tool for quick access to files and directories
brew "fasd"
# Play, record, convert, and stream audio and video
brew "ffmpeg"
# Banner-like program prints strings as ASCII art
brew "figlet"
# Infamous electronic fortune-cookie generator
brew "fortune"
# Monitor a directory for changes and run a shell command
brew "fswatch"
# Command-line fuzzy finder written in Go
brew "fzf"
# GNU debugger
brew "gdb"
# GitHub command-line tool
brew "gh"
# Distributed revision control system
brew "git"
brew "zsh-syntax-highlighting"
# Git extension for versioning large files
brew "git-lfs"
# SDK for signal processing blocks to implement software radios
brew "gnuradio"
# Open source programming language to build simple/reliable/efficient software
brew "go"
# Open-source build automation tool based on the Groovy and Kotlin DSL
brew "gradle"
# Bash and Zsh completion for Gradle
brew "gradle-completion"
# Graph visualization software from AT&T and Bell Labs
brew "graphviz"
# SDK for Groovy: a Java-based scripting language
brew "groovysdk"
# Development framework for multimedia applications
brew "gstreamer"
# Low cost software radio platform
brew "hackrf"
# Integration point for ghcide and haskell-ide-engine. One IDE to rule them all
brew "haskell-language-server"
# Cross-platform program for developing Haskell projects
brew "haskell-stack"
# Haskell source code suggestions
brew "hlint"
# User-friendly cURL replacement (command-line HTTP client)
brew "httpie"
# Add GitHub support to git on the command-line
brew "hub"
# Tools and libraries to manipulate images in many formats
brew "imagemagick"
# Lightweight and flexible command-line JSON processor
brew "jq"
# Build tool for Clojure
brew "leiningen"
# Hash utilities
brew "md5sha1sum", link: false
# Scalable distributed version control system
brew "mercurial"
# MP3 player for Linux and UNIX
brew "mpg123"
# Maven version manager
brew "mvnvm"
# Open source relational database management system
brew "mysql"
# FTP client with an advanced user interface
brew "ncftp"
# Fast, highly customisable system info script
brew "neofetch"
# Ambitious Vim-fork focused on extensibility and agility
brew "neovim"
# Platform built on V8 to build network applications
brew "node"
# node version manager
brew "nvm"
# 7-Zip (high compression file archiver) implementation
brew "p7zip"
# Cross-platform library for real-time MIDI I/O
brew "portmidi"
# Object-relational database system
brew "postgresql"
# Python version management
brew "pyenv"
# Powerful, clean, object-oriented scripting language
brew "ruby"
# Build tool for Scala projects
brew "sbt"
# JVM-based programming language
brew "scala"
# Software for mathematics, science, and engineering
brew "scipy"
# Count lines of code in many languages
brew "sloccount"
# SOcket CAT: netcat on steroids
brew "socat"
# Sparklines for the shell
brew "spark"
# Cross-shell prompt for astronauts
brew "starship"
# CLI for extracting streams from various websites to a video player
brew "streamlink"
# Separate tracks via Shoutcasts title-streaming
brew "streamripper"
# Version control system designed to be a better CVS
brew "subversion"
# Open source continuous file synchronization application
brew "syncthing", restart_service: true
# User interface to the TELNET protocol
brew "telnet"
# Send macOS User Notifications from the command-line
brew "terminal-notifier"
# Programmatically correct mistyped console commands
brew "thefuck"
# Terminal multiplexer
brew "tmux"
# Anonymizing overlay network for TCP
brew "tor"
# Extract, view, and test RAR archives
# unrar no longer supported by homebrew due to licensing use 7z for rar archives as per:
# https://github.com/Homebrew/discussions/discussions/285
#brew "unrar"
# Executes a program periodically, showing output fullscreen
brew "watch"
# Internet file retriever
brew "wget"
# JavaScript package manager
brew "yarn"
# Fork of youtube-dl with additional features and fixes
brew "yt-dlp"
# Set of tools and dependencies for plugins on Atlassian server applications
brew "atlassian/tap/atlassian-plugin-sdk"
brew "sudar/arduino-mk/arduino-mk"
brew "websocat"
brew "ripgrep"
brew "ranger"
# better find
brew "fd"
# better cat
brew "bat"
brew "bat-extras"

# js/ts dev package manager tool 
brew "pnpm"


#vectorbrat, audio

brew "jack"
# gui for controlling jack audio connection kit
brew "qjackctl"



# casks

cask "chromium"
# App to build and share containerized applications and microservices
cask "docker"
# System-wide audio equalizer
cask "eqmac"
# Web browser
cask "firefox"

# fonts
# very nice coding and terminal font with ligature support
cask "font-fira-code"
cask "font-jetbrains-mono-nerd-font"

# Software-defined radio receiver powered by GNU Radio and Qt
cask "gqrx"
# Hex editor focussing on speed
cask "hex-fiend"
# retro emulation
cask "mame"
# Virtualizer for x86 hardware (does not work on apple silicon as of 220220716)
#cask "virtualbox"
# Desktop client for WhatsApp
cask "whatsapp"
cask "iterm2"
cask "caffeine"
cask "rectangle"
cask "discord"
cask "vlc"
cask "spotify"
cask "chromium"
cask "docker"
cask "obs"
cask "arduino-ide"
# retro terminal emulator won't install as of 20220716
#mas "Cathode", id: 499233976
mas "Flycut", id: 442160987
mas "iMovie", id: 408981434
mas "Keynote", id: 409183694
mas "Numbers", id: 409203825
mas "Pages", id: 409201541
mas "Slack", id: 803453959
mas "The Witness", id: 1167726849
mas "Things", id: 904280696
mas "Xcode", id: 497799835
mas "1Password 7 - Password Manager", id: 1333542190
